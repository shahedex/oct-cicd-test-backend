#!/bin/bash

kill -9 $(lsof -t -i:5000)

cd /home/cicd/two-tier-application/backend
git pull --rebase
pip3 install -r requirements.txt
which flask
echo "after path edit"
export PATH="$PATH:/home/cicd/.local/bin"
which flask
flask run --host 0.0.0.0 &
exit 0
